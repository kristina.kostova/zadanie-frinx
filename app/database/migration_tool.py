"""Module to run migrations programmatically (i.e., to invoke them from python code)."""
import logging
import pathlib

from alembic import command
from alembic.config import Config

from app.core.settings import settings

logger = logging.getLogger(__name__)


class AlembicMigrationTool:
    """Wrapper for generate alembic migrations."""

    database_folder: pathlib.Path = pathlib.Path(__file__).parent.resolve().__str__()  # type:ignore[assignment]
    alembic_ini: pathlib.Path = f'{database_folder}/alembic.ini'  # type:ignore[assignment]

    def __init__(self, script_location: str = "/migrations") -> None:
        """Initialize migration tool."""
        self.db_url = settings.DATABASE_URL
        self.config = self.generate_alembic_config(self.db_url, script_location=script_location)

    def generate_alembic_config(self, db_url: str, script_location: str) -> Config:
        """
        Generate alembic config.

        @param db_url: database url for running migrations.
        @param script_location: location of migrations
        @return: Config object for Alembic Migration Tool

        """
        alembic_cfg = Config(file_=self.alembic_ini)  # type:ignore[arg-type]
        alembic_cfg.set_main_option("sqlalchemy.url", db_url)
        alembic_cfg.set_main_option('script_location', f'{self.database_folder}{script_location}')
        return alembic_cfg

    def upgrade(self, revision_hash: str = "head") -> None:
        """Upgrade the database to the specific revision via alembic tools."""
        command.upgrade(self.config, revision_hash)

    def downgrade(self, revision_hash: str) -> None:  # type:ignore[assignment]
        """Downgrade the database to the specific revision via alembic tools."""
        command.downgrade(self.config, revision_hash)

    def revision(self, message: str, autogenerate: bool = True) -> None:  # type:ignore[assignment]
        """Autogenerate the migrations for database. Create a new revision file."""
        autogenerate = eval(autogenerate) if not type(autogenerate) == bool else autogenerate  # type:ignore[arg-type]
        command.revision(self.config, message, autogenerate)

    def stamp(self, revision_hash) -> None:
        """Stamp the revision table with the given revision; don’t run any migrations."""
        command.stamp(self.config, revision_hash)
