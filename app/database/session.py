import logging
from contextlib import asynccontextmanager, contextmanager
from functools import lru_cache
from typing import AsyncGenerator, Callable, Generator

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.session import Session

from .database import get_engine, get_async_engine

logger = logging.getLogger(__name__)


@lru_cache(maxsize=1)
def get_session_maker() -> sessionmaker:
    """Get the sessionmaker.

    Note how the lru cache will make sure that the same instance is returned on every call.

    The advantage of this pattern is that the resource is only initialized on usage, not
    on module import.

    """
    return sessionmaker(bind=get_engine())


@lru_cache(maxsize=1)
def get_async_session_maker() -> sessionmaker:
    """Get the async sessionmaker.

    Note how the lru cache will make sure that the same instance is returned on every call.

    The advantage of this pattern is that the resource is only initialized on usage, not
    on module import.

    """
    return sessionmaker(get_async_engine(), class_=AsyncSession, expire_on_commit=False)


def _safe_session_handler(session: Session) -> Generator[Session, None, None]:
    try:
        yield session
        session.commit()
    except Exception as exception:
        logger.error("Exception occured while running DB code, attempting rollback", extra={"exception": exception})
        session.rollback()
    finally:
        session.close()


def get_db_session() -> Generator[Session, None, None]:
    """Get a db session handle for CRUD, close it automatically once done."""
    session_maker = get_session_maker()
    session = session_maker()
    yield from _safe_session_handler(session)


async def get_db_session_async() -> AsyncGenerator[AsyncSession, None]:
    """Get an async db session handle.

    This is mostly intended for user management, see
    https://fastapi-users.github.io/fastapi-users/configuration/databases/sqlalchemy/#create-the-database-adapter-dependency

    """
    session_maker = get_async_session_maker()
    async with session_maker() as session:
        yield session


get_db_session_as_context_manager: Callable = contextmanager(get_db_session)  # type: ignore
get_db_session_as_async_context_manager: Callable = asynccontextmanager(get_db_session_async)  # type: ignore
