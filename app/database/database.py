from functools import lru_cache

from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.ext.asyncio import AsyncEngine, create_async_engine
from sqlalchemy.orm import declarative_base
from sqlalchemy.pool import QueuePool


from app.core.settings import settings

Base = declarative_base()
META_DATAS = [Base.metadata]


def get_engine_queue_pool_config(is_for_sync_driver: bool = True) -> dict:
    """Get the database connection pool class for engines to use."""
    shared_options = {"poolclass": QueuePool}
    if is_for_sync_driver:
        return {**shared_options,
                "pool_size": settings.DATABASE_POOL_SIZE_FOR_SYNC_DRIVER,
                "max_overflow": settings.DATABASE_MAX_POOL_SIZE_FOR_SYNC_DRIVER,
                "pool_recycle": settings.DATABASE_POOL_RECYCLE,
                "pool_timeout": settings.DATABASE_POOL_TIMEOUT
                }
    else:
        return {**shared_options,
                "pool_size": settings.DATABASE_POOL_SIZE_FOR_ASYNC_DRIVER,
                "max_overflow": settings.DATABASE_MAX_POOL_SIZE_FOR_ASYNC_DRIVER,
                "pool_recycle": settings.DATABASE_POOL_RECYCLE,
                }


@lru_cache()
def get_engine() -> Engine:
    """Get the engine.

    Note how the lru cache will make sure that the same instance is returned on every call.

    The advantage of this pattern is that the resource is only initialized on usage, not
    on module import.

    """
    dsn = settings.DATABASE_URL
    engine: Engine = create_engine(dsn, **get_engine_queue_pool_config(), pool_pre_ping=True)
    return engine


@lru_cache(maxsize=1)
def get_async_engine() -> AsyncEngine:
    """Get the async engine.

    Note how the lru cache will make sure that the same instance is returned on every call.

    The advantage of this pattern is that the resource is only initialized on usage, not
    on module import.

    """
    return create_async_engine(settings.get_database_url_async(),
                               **get_engine_queue_pool_config(is_for_sync_driver=False))


def create_all_skipping_migrations() -> None:
    """Create all sql resources while skipping migrations.

    This helper is useful for development.
    """
    for meta_data in META_DATAS:
        meta_data.create_all(get_engine())
