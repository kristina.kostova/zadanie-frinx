from app.database.migration_tool import AlembicMigrationTool
try:
    AlembicMigrationTool().upgrade()
except Exception as e:
    print(e)
    pass
