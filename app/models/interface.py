from sqlalchemy import Column, Integer, String, JSON
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import class_mapper

Base = declarative_base()


class Interface(Base):
    __tablename__ = 'interfaces'

    id = Column(Integer, primary_key=True, autoincrement=True)
    connection = Column(Integer)
    name = Column(String(255), nullable=False)
    description = Column(String(255))
    config = Column(JSON)
    type = Column(String(50))
    infra_type = Column(String(50))
    port_channel_id = Column(Integer)
    max_frame_size = Column(Integer)

    def to_dict(self):
        return {c.key: getattr(self, c.key)
                for c in class_mapper(self.__class__).columns}
