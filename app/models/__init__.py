from .interface import Interface as InterfaceModel, Base

META_DATAS = [Base.metadata]

__all__ = [
    'InterfaceModel',
]
