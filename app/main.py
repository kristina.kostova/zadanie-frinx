from fastapi import FastAPI, HTTPException, status
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import JSONResponse

from app.api import routers
from app.core import settings, setup_logging
from app.core.parse_interface import load_interfaces_to_database
from apscheduler.schedulers.background import BackgroundScheduler


setup_logging()

# FastAPI app
app = FastAPI(
    version="0.1.0",
    description="Zadanie Finxgit "
)
origins = [
    "http://localhost:8000",  # CORS to localhost origin
    # "*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# Exception handling
@app.exception_handler(HTTPException)
async def http_exception_handler(request, exc):
    return JSONResponse(
        status_code=exc.status_code,
        content={"message": exc.detail},
    )


@app.exception_handler(Exception)
async def exception_handler(request, exc):
    return JSONResponse(
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        content={"message": "An unexpected error occurred."},
    )


@app.on_event("startup")
async def start_scheduler():
    scheduler = BackgroundScheduler()
    scheduler.add_job(load_interfaces_to_database, 'interval', seconds=10)
    scheduler.start()

app.include_router(routers.api_router, prefix=settings.API_V1_PREFIX)
