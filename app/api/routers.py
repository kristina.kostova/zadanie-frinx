import logging

from fastapi import APIRouter, HTTPException
from fastapi.params import Depends
from sqlalchemy import text
from sqlalchemy.orm import Session

from app.database.session import get_db_session
from app.api.endpoints.interface import router as interface_router

api_router = APIRouter()
api_router.include_router(interface_router)

logger = logging.getLogger(__name__)


@api_router.get("/health")
def health(session: Session = Depends(get_db_session)):
    """
    Health check endpoint of backend application.

    @param session: This is a parameter that is used to maintain a connection to the database
    """
    try:
        session.execute(text('SELECT 1 AS is_alive;'))
    except Exception as e:
        logger.error(e)
        raise HTTPException(503, detail="Health check failed")
    return {"message": "OK"}
