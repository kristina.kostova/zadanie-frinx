import logging
from typing import List

from fastapi import Depends, APIRouter, HTTPException
from sqlalchemy.orm import Session

from app.crud.interface import get_interfaces, get_interface
from app.database.session import get_db_session
from app.schemas import InterfaceSchema

router = APIRouter(prefix="/interfaces", tags=["interfaces"])
logger = logging.getLogger('test')


@router.get("/", response_model=List[InterfaceSchema])
def get_all_interfaces(session: Session = Depends(get_db_session)) -> List[InterfaceSchema]:
    """
    Endpoint to get all interfaces.

    @param session: This is a parameter that is used to
     maintain a connection to the database
    @return: list of interfaces
    """
    return [interface.to_dict() for interface in get_interfaces(session=session)]


@router.get("/{interface_id}", response_model=InterfaceSchema | None)
def get_interface_by_id(
        interface_id: int,
        session: Session = Depends(get_db_session)
) -> InterfaceSchema | str:
    """
    Endpoint to retrieve information about an interface based on its ID.

    @param interface_id:  A specific ID that identifies the interface being requested.
    @param session: This is a parameter that is used to maintain a connection to the database
    @return:  The requested interface in JSON format
    """
    if interface := get_interface(interface_id=interface_id, session=session):
        return interface
    raise HTTPException(404, "Interface not found.")
