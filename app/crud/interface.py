import logging
from typing import List

from sqlalchemy.orm import Session, Query

from app.database import get_db_session_as_context_manager
from app.models import InterfaceModel

logger = logging.getLogger('crud interface')


def get_interfaces(session: Session) -> List:
    """
    Provides a query of all interfaces.

    @param session: A database session object that is used to interact with the database.
    @return: Query of all interfaces
    """
    return session.query(InterfaceModel).all()


def get_interface(interface_id: int, session: Session) -> InterfaceModel:
    """
    Provides an interface filtered interface_id movie_id.

    @param interface_id: ID of the interface to be retrieved.
    @param session: A database session object that is used to interact with the database.
    @return: Interface object
    """
    return session.query(InterfaceModel).filter(InterfaceModel.id == interface_id).first()


def get_interface_by_name(interface_name: str, session: Session) -> InterfaceModel:
    """
    Provides an interface filtered by name.

    @param interface_name: A string that contains the name of the interface to be retrieved.
    @param session: A database session object that is used to interact with the database.
    @return: Interface object
    """
    return session.query(InterfaceModel).filter(InterfaceModel.name == interface_name).first()


def get_port_channel_id(port_channel_configuration: dict | None) -> int | None:
    """
    Retrieves the ID of a specific Port-channel interface from the database.

    :param port_channel_configuration: A dictionary containing port channel configuration or None.
    :return: Returns the ID of the interface if found, else None.
    """
    if port_channel_configuration:
        with get_db_session_as_context_manager() as db:
            interface = get_interface_by_name('Port-channel' + str(port_channel_configuration.get('number')), db)
            return interface.id if interface else None
    return None
