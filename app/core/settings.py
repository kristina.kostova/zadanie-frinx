from pathlib import Path
from pydantic import BaseSettings
from functools import lru_cache

# Project root directory
BASE_DIR = Path(__file__).resolve().parent.parent

# FastAPI settings
API_PREFIX = "/api"
API_V1_PREFIX = f"{API_PREFIX}/v1"
PROJECT_NAME = "seznam-films"


class Settings(BaseSettings):
    """Settings for the application."""
    DATABASE_URL: str = f"sqlite:///{BASE_DIR / 'database.sqlite3'}"
    DATABASE_POOL_SIZE_FOR_SYNC_DRIVER: int = 10
    DATABASE_MAX_POOL_SIZE_FOR_SYNC_DRIVER: int = 20
    DATABASE_POOL_RECYCLE: int = 180
    DATABASE_POOL_TIMEOUT: int = 300

    DATABASE_POOL_SIZE_FOR_ASYNC_DRIVER: int = 15
    DATABASE_MAX_POOL_SIZE_FOR_ASYNC_DRIVER: int = 25
    class Config:
        """Configuration of the settings."""

        env_file = ".env"
        env_file_encoding = "utf-8"


@lru_cache()
def get_settings():
    """Getting a Settings class."""
    return Settings()


settings = get_settings()
