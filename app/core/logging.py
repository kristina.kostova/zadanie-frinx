import logging
import sys


def setup_logging():
    """Setup of logger in aplication."""
    logger = logging.getLogger('interfaces')

    format = '%(levelname)s %(asctime)s %(filename)s:%(lineno)d %(message)s'
    logger.setLevel(logging.DEBUG)

    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter(format)
    handler.setFormatter(formatter)

    logger.addHandler(handler)
    logger.debug(f'Logger {__name__} active.')
