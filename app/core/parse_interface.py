import json
import logging

from app.crud.interface import get_port_channel_id
from app.database import get_db_session_as_context_manager
from app.models import InterfaceModel
from app.schemas.interface import InterfaceCreate

logger = logging.getLogger('interfaces')


def find_in_dict(obj: dict, key: str):
    """
    Generator function to recursively find all values of the specified key in a nested dictionary.

    :param obj: A dictionary object in which to search for the key.
    :param key: The key to be searched for within the dictionary.
    :yield: Yields the values of the specified key found in the dictionary.
    """
    if key in obj:
        yield obj[key]
    for k, v in obj.items():
        if isinstance(v, dict):
            yield from find_in_dict(v, key)
        elif isinstance(v, list):
            for item in v:
                if isinstance(item, dict):
                    yield from find_in_dict(item, key)


def create_interfaces(config: dict, interface_data: dict, type_interface: str) -> None:
    """
    Creates or updates Interface objects in the database based on the provided interface data.
    Each Interface object is created or updated according to the corresponding dictionary in interface_data.

    If an Interface object with a given name doesn't exist in the database, a new one is created.
    If an Interface object with a given name already exists in the database, it's updated with new data.

    :param config: A dictionary containing interface configurations.
                   It's expected to have 'config' and 'type' keys whose values are used
                   to set corresponding attributes of Interface objects.

    :param interface_data: A list of dictionaries, each containing data to create or update an Interface object.
                           Each dictionary is expected to have 'name', 'description', 'mtu', and
                           "Cisco-IOS-XE-ethernet:channel-group" keys.

    :param type_interface: A string specifying the type of interfaces to be created or updated.
                           It's prepended to the 'name' key of each dictionary in interface_data
                           to form the 'name' attribute of corresponding Interface objects.

    :return: None. The function directly commits changes to the database.
    """
    with get_db_session_as_context_manager() as db:
        for interface in interface_data:
            if is_dict_with_name(interface):
                interface_model_dict = InterfaceCreate(
                    name=type_interface + str(interface.get('name')),
                    description=interface.get('description'),
                    config=get_attribute_by_name(config, 'config', type_interface + str(interface.get('name'))),
                    type=get_attribute_by_name(config, 'type', type_interface + str(interface.get('name'))),
                    max_frame_size=interface.get('mtu'),
                    port_channel_id=get_port_channel_id(interface.get("Cisco-IOS-XE-ethernet:channel-group"))
                ).dict()
                db_interface = db.query(InterfaceModel).filter(
                    InterfaceModel.name == interface_model_dict["name"]).first()

                if db_interface is None:
                    # Create new interface
                    db.add(InterfaceModel(**interface_model_dict))
                else:
                    # Update existing interface
                    for key, value in interface_model_dict.items():
                        setattr(db_interface, key, value)
            db.commit()


def is_dict_with_name(value: str) -> bool:
    """
       Verifies if the input value is a dictionary containing 'name' key.

       :param value: Expected to be a dictionary.
       :return: True if 'value' is a dictionary with 'name' key, False otherwise.
    """
    return isinstance(value, dict) and 'name' in value


def get_attribute_by_name(config: dict, attribute: str, name: str) -> str | None:
    """
    Extracts the specified attribute's value from an interface with the given name.

    :param config: A dictionary expected to contain interface configurations.
    :param attribute: The attribute name to be extracted.
    :param name: The name of the specific interface from which to extract the attribute.
    :return: The attribute's value if found, else None.
    """
    for interface_list in find_in_dict(config, 'interface'):
        for interface in interface_list:
            if is_dict_with_name(interface):
                if interface['name'] == name and attribute in interface:
                    return interface.get(attribute)
    return None


def load_interfaces_to_database() -> None:
    """
    Loads config file and store interface information to database
    """
    logger.debug('running load interface')
    with open('./app/initial_data/config.json') as f:
        config = json.load(f)

    # Create instances of the Interface model and add them to the session
    list_of_interfaces = {'BDI', 'Loopback', 'Port-channel', 'TenGigabitEthernet', 'GigabitEthernet'}
    for interface in list_of_interfaces:
        for interface_list in find_in_dict(config, interface):
            create_interfaces(config, interface_list, interface)
