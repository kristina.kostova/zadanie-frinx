from typing import Optional

from pydantic import BaseModel


class InterfaceCreate(BaseModel):
    name: str
    description: Optional[str]
    config: Optional[dict]
    type: Optional[str]
    infra_type: Optional[str]
    port_channel_id: Optional[int]
    max_frame_size: Optional[int]


class InterfaceSchema(InterfaceCreate):
    id: int
