# Zadanie Frinx

### Prerequisites:
    - Python 3.6+
    - PostgreSQL 10+
### Database model:
    - id SERIAL PRIMARY KEY,
    - connection INTEGER,
    - name VARCHAR(255) NOT NULL,
    - description VARCHAR(255),
    - config json,
    - type VARCHAR(50), 
    - infra_type VARCHAR(50),
    - port_channel_id INTEGER,
    - max_frame_size INTEGER
### Description:
You need to extract device interface configuration from config.json file and store relevant
data to database. There are 10 BDI, 1 Loopback, 2 Port-channel, 4 TenGigabitEthernet
and 3 GigabitEthernet interfaces.
We are interested only on Port-channels and Ethernet interfaces. BDI and Loopback can
be ignored for now, but your solution should be able to handle BDI and Loopback in future.
In database we want to fill this fields, other can be null:
    - id
    - name (interface group name + interface name, for example "TenGigabitEthernet0/0/5");
    - description (optional, interface description, for example "member of Portchannel20");
    - max_frame_size (optional, mtu from interface configuration)
    - config (whole interface configuration) 
    - port_channel_id link Ethernet interfaces to Port-channel. This is defined in configuration by: "Cisco-IOS-XE-ethernet:channel-group"


## Project Setup and Container Description
This project is dockerized and set up via **docker-compose**. 
Below is a brief description of how to run the project and the role of each container.

### How to Run the Project
You can start the entire project with a single command:

```bash
docker-compose up --build
```
This command starts all services defined in the docker-compose.yml file. 
The **--build** option ensures that Docker builds the images before starting the containers.


## PGADMIN

```bash
http://0.0.0.0:5050/
```
email: *postgres@postgres.com*
password: *postgres*



## Accessing the Frontend and Backend

Once the project is successfully launched, the frontend and backend can be accessed in your browser at the following addresses:

### Backend

The backend of the application runs on port 8080. FastAPI comes with a built-in interactive documentation tool called Swagger UI. To see the Swagger UI and experiment with various API endpoints, enter the following address into your browser:

```bash
http://0.0.0.0:8080/docs
```

This page allows you to see all the available API endpoints, their expected input and output structures, and even allows you to test the API directly in the browser.



