FROM python:3.11.3-slim-buster  as base

# Create a new user and set the working directory
RUN useradd -m -s /bin/bash appuser
USER appuser

# Set the working directory
WORKDIR /home/appuser/src

# Copy requirements.txt and install Python dependencies
COPY --chown=appuser:appuser requirements/base.txt ./base.txt
RUN pip install --no-cache-dir --user -r ./base.txt

# Copy the project into the container
COPY --chown=appuser:appuser ./app ./app
COPY --chown=appuser:appuser ./app/run_migration.py ./run_migration.py
# Add the local Python packages to the PATH
ENV PATH="/home/appuser/.local/bin:${PATH}"

# Copy the project into the container
COPY ./app ./app

CMD python run_migration.py && uvicorn app.main:app --reload --host 0.0.0.0 --port 8080



FROM base as  test

COPY --chown=appuser:appuser requirements/local.txt ./local.txt
RUN pip install --no-cache-dir --user -r ./local.txt
COPY --chown=appuser:appuser ./pytest.ini  ./pytest.ini
COPY --chown=appuser:appuser ./.flake8  ./.flake8

CMD pytest && flake8